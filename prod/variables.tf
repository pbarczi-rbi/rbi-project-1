variable "env" {
  description = "The Name of the Environment"
  default = "undefined"
}

variable "instance_type" {
  description = "Default EC2 Instane Type"
  default = "t2.micro"
}
