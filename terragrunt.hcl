remote_state {
  backend = "s3"
  config = {
    bucket  = "pety-tf"
    key     = "${path_relative_to_include()}/terraform.tfstate"
    region  = "eu-central-1"
  }
}
