include "root "{
  path = find_in_parent_folders()
}

inputs = {
  instance_type  = "t2.micro"
  env            = "dev"
}
