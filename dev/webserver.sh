#!/bin/bash
# This Script creates webserver.sh script whic will be placed on destination EC2.
# Such script is responsible for implementing webserver via python and serving static
# web page containing Instance info.

# Create script
cat <<SCRIPT >/var/tmp/webserver.sh
cd /var/tmp

# Variables
IP=\`curl --silent http://169.254.169.254/latest/meta-data/local-ipv4\`
MAC=\`ip a s eth0 | grep ether | awk '{print \$2}'\`
SUBNET=\`curl --silent http://169.254.169.254/latest/meta-data/network/interfaces/macs/\$MAC/subnet-ipv4-cidr-block\`
ENV=dev

# Create content of index.html
echo "Hello from \$ENV. My \$IP lies in \$SUBNET." > /var/tmp/index.html

# Start webserver
python -m SimpleHTTPServer 80
SCRIPT

# Autostart of webserver
chmod +x /var/tmp/webserver.sh
echo "/var/tmp/webserver.sh" >> /etc/rc.local
chmod +x /etc/rc.d/rc.local
/var/tmp/webserver.sh
#
